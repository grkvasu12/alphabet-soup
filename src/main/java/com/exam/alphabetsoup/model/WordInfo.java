package com.exam.alphabetsoup.model;

public class WordInfo {

    String word;

    Integer startRow;
    Integer endRow;
    Integer startColumn;
    Integer endColumn;
    String wordWithoutSapce;

    boolean wordPresent = false;

    Integer startIndex;

    Integer endIndex;

    public Integer getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(Integer startIndex) {
        this.startIndex = startIndex;
    }

    public Integer getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(Integer endIndex) {
        this.endIndex = endIndex;
    }

    public WordInfo(String word, Integer startRow, Integer endRow, Integer startColumn, Integer endColumn, boolean wordPresent) {
        this.word = word;
        this.startRow = startRow;
        this.endRow = endRow;
        this.startColumn = startColumn;
        this.endColumn = endColumn;
        this.wordPresent = wordPresent;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Integer getStartRow() {
        return startRow;
    }

    public void setStartRow(Integer startRow) {
        this.startRow = startRow;
    }

    public Integer getEndRow() {
        return endRow;
    }

    public void setEndRow(Integer endRow) {
        this.endRow = endRow;
    }

    public Integer getStartColumn() {
        return startColumn;
    }

    public void setStartColumn(Integer startColumn) {
        this.startColumn = startColumn;
    }

    public Integer getEndColumn() {
        return endColumn;
    }

    public void setEndColumn(Integer endColumn) {
        this.endColumn = endColumn;
    }

    public boolean isWordPresent() {
        return wordPresent;
    }

    public void setWordPresent(boolean wordPresent) {
        this.wordPresent = wordPresent;
    }

    public WordInfo(String word) {
        this.word = word;
    }

    public String getWordWithoutSapce() {
        return this.word.replaceAll("\\s","");
    }

    public void setWordWithoutSapce(String wordWithoutSapce) {
        this.wordWithoutSapce = wordWithoutSapce;
    }
}
