package com.exam.alphabetsoup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlphabetsoupApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlphabetsoupApplication.class, args);
    }

}
