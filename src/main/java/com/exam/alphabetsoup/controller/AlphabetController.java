package com.exam.alphabetsoup.controller;

import com.exam.alphabetsoup.model.WordInfo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@RestController
public class AlphabetController {

    @RequestMapping(value = "/upload", method = RequestMethod.POST, produces = "application/json")
    public String uploadFile(@RequestParam("file") MultipartFile inputFile) throws IOException {
        String responseString="";
        List<WordInfo> wordInfos = new ArrayList<>();
        Map<Integer, String> matrixMap = new HashMap<>();

        Scanner scanner = new Scanner(inputFile.getInputStream());
            String[] firstLine = scanner.nextLine().trim().split("x");
            int rows = Integer.parseInt(firstLine[0]);
            int columns = Integer.parseInt(firstLine[1]);
            Integer mapCounter =1;
            for (int i=0; i<rows; i++) {
                String[] line = scanner.nextLine().trim().split(" ");
                for (int j=0; j<line.length; j++) {
                    matrixMap.put(mapCounter++, line[j]);
                }
            }
            while(scanner.hasNextLine()){
                String line = scanner.nextLine().trim();
                if(line==null || line.equals("")) break;
                wordInfos.add(new WordInfo(line));
            }
            System.out.println("wordsToSearch>>>>" +wordInfos.size() );

            for(WordInfo wordInfo : wordInfos){
                // Forward search
                    searchVertical(matrixMap,wordInfo, columns, rows*columns);
                    searchHorizontal(matrixMap,wordInfo, columns, rows*columns);
                    searchDiagonal(matrixMap,wordInfo, columns, rows*columns);
                  // backwardSearch
                    searchBackwardVertical(matrixMap,wordInfo, columns, rows*columns);
                    searchBackwardHorizontal(matrixMap,wordInfo, columns, rows*columns);
                    searchBackwardDiagonal(matrixMap,wordInfo, columns, rows*columns);

                    responseString+= wordInfo.getWord() + " " + buildMatrixIndex(columns, wordInfo.getStartIndex()) + " " + buildMatrixIndex(columns, wordInfo.getEndIndex()) + " \n";
            }
            System.out.println("\n\n\n************************************************************\n");
                System.out.println(responseString);
            System.out.println("************************************************************");

            return responseString;
    }

    public WordInfo searchVertical(Map<Integer, String> matrixMap, WordInfo wordInfo, Integer columns, Integer totalChars){

        if(wordInfo.isWordPresent()) return wordInfo;
        Integer firstCharIndex;

        outer: for(Integer i=1; i<=totalChars; i++){
            String wordToSearch = wordInfo.getWordWithoutSapce();
            if(wordToSearch.charAt(0)==matrixMap.get(i).charAt(0)){
                //System.out.println("first letter found at--" + i);
                firstCharIndex=i;
                if(wordToSearch.length()==1) {
                    setInfoToWordObject(wordInfo, true, firstCharIndex, firstCharIndex);
                    break outer;
                }
                for(Integer j=1; j<wordToSearch.length(); j++) {
                    Integer mapIndex=firstCharIndex+(columns*j);
                    if((mapIndex<=totalChars && wordToSearch.charAt(j)!= matrixMap.get(mapIndex).charAt(0)) || mapIndex>totalChars){
                        wordInfo.setWordPresent(false);
                        break;
                    }
                    if(j==wordToSearch.length()-1) {
                        setInfoToWordObject(wordInfo,true,firstCharIndex,mapIndex);
                        break outer;
                    }
                }
            }
        }
        return wordInfo;
    }

    public WordInfo searchHorizontal(Map<Integer, String> matrixMap, WordInfo wordInfo,Integer columns, Integer totalChars){
        if(wordInfo.isWordPresent()) return wordInfo;
        Integer firstCharIndex;

        outer: for(Integer i=1; i<=totalChars; i++){
            String wordToSearch = wordInfo.getWordWithoutSapce();
            if(wordToSearch.charAt(0)==matrixMap.get(i).charAt(0)){
               // System.out.println("first letter found at--" + i);
                if(((i-1)%columns + wordToSearch.length())>columns){
                    continue; // horizontal is not possible
                }
                firstCharIndex=i;
                if(wordToSearch.length()==1) {
                    setInfoToWordObject(wordInfo, true, firstCharIndex, firstCharIndex);
                    break outer;
                }
                for(Integer j=1; j<wordToSearch.length(); j++) {
                    Integer mapIndex=firstCharIndex+j;
                    if((mapIndex<=totalChars && wordToSearch.charAt(j)!= matrixMap.get(mapIndex).charAt(0)) || mapIndex>totalChars){
                        wordInfo.setWordPresent(false);
                        break;
                    }
                    if(j==wordToSearch.length()-1) {
                        setInfoToWordObject(wordInfo, true, firstCharIndex, mapIndex);
                        break outer;
                    }
                }
            }
        }
        return wordInfo;
    }

    public WordInfo searchDiagonal(Map<Integer, String> matrixMap, WordInfo wordInfo, Integer increment, Integer totalChars){
        return searchVertical(matrixMap, wordInfo, increment+1, totalChars);
    }

    public WordInfo searchBackwardVertical(Map<Integer, String> matrixMap, WordInfo wordInfo, Integer columns, Integer totalChars){

        if(wordInfo.isWordPresent()) return wordInfo;
        Integer firstCharIndex;

        outer: for(Integer i=1; i<=totalChars; i++){
            String wordToSearch = wordInfo.getWordWithoutSapce();
            if(wordToSearch.charAt(0)==matrixMap.get(i).charAt(0)){
                //System.out.println("first letter found at--" + i);
                firstCharIndex=i;
                if(wordToSearch.length()==1) {
                    setInfoToWordObject(wordInfo, true, firstCharIndex, firstCharIndex);
                    break outer;
                }
                for(Integer j=1; j<wordToSearch.length(); j++) {
                    Integer mapIndex=firstCharIndex-(columns*j);
                    if((mapIndex>0 && wordToSearch.charAt(j)!= matrixMap.get(mapIndex).charAt(0)) || mapIndex<1){
                        wordInfo.setWordPresent(false);
                        break;
                    }
                    if(j==wordToSearch.length()-1) {
                        setInfoToWordObject(wordInfo,true,firstCharIndex,mapIndex);
                        break outer;
                    }
                }
            }
        }
        return wordInfo;
    }

    public WordInfo searchBackwardHorizontal(Map<Integer, String> matrixMap, WordInfo wordInfo,Integer columns, Integer totalChars){
        if(wordInfo.isWordPresent()) return wordInfo;
        Integer firstCharIndex;

        outer: for(Integer i=1; i<=totalChars; i++){
            String wordToSearch = wordInfo.getWordWithoutSapce();
            if(wordToSearch.charAt(0)==matrixMap.get(i).charAt(0)){
                // System.out.println("first letter found at--" + i);
                if(i%columns < wordToSearch.length()){
                    continue; // horizontal is not possible
                }
                firstCharIndex=i;
                if(wordToSearch.length()==1) {
                    setInfoToWordObject(wordInfo, true, firstCharIndex, firstCharIndex);
                    break outer;
                }
                for(Integer j=1; j<wordToSearch.length(); j++) {
                    Integer mapIndex=firstCharIndex-j;
                    if((mapIndex>0 && wordToSearch.charAt(j)!= matrixMap.get(mapIndex).charAt(0)) || mapIndex<1){
                        wordInfo.setWordPresent(false);
                        break;
                    }
                    if(j==wordToSearch.length()-1) {
                        setInfoToWordObject(wordInfo, true, firstCharIndex, mapIndex);
                        break outer;
                    }
                }
            }
        }
        return wordInfo;
    }

    public WordInfo searchBackwardDiagonal(Map<Integer, String> matrixMap, WordInfo wordInfo, Integer increment, Integer totalChars){
        return searchBackwardVertical(matrixMap, wordInfo, increment+1, totalChars);
    }

    public void setInfoToWordObject(WordInfo wordObject, boolean wordPresent, Integer startIndex, Integer endIndex){
        wordObject.setWordPresent(wordPresent);
        wordObject.setStartIndex(startIndex);
        wordObject.setEndIndex(endIndex);
    }

    public String buildMatrixIndex(Integer columns, Integer index){
        if(index==null) return "NA:NA";
        Integer row = (index-1)/columns;
        Integer column = (index-1)% columns;
        return row.toString() + ":" +column.toString();
    }

}
