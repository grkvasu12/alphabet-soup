package com.exam.alphabetsoup;

import com.exam.alphabetsoup.controller.AlphabetController;
import com.exam.alphabetsoup.model.WordInfo;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class AlphabetControllerTest {

    AlphabetController controller = new AlphabetController();
    @Test
    public void test_searchVertical_word_available_1(){
        WordInfo wordInfo = new WordInfo("HEEL");
        Integer increment = 5;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode = controller.searchVertical(matrixMap,wordInfo,increment, 25);
        assert (outputFromCode.getStartIndex()==1);
        assert (outputFromCode.getEndIndex()==16);
        assert (outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchVertical_word_available_2(){
        WordInfo wordInfo = new WordInfo("GLHU");
        Integer increment = 5;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode = controller.searchVertical(matrixMap,wordInfo,increment, 25);
        assert (outputFromCode.getStartIndex()==7);
        assert (outputFromCode.getEndIndex()==22);
        assert (outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchVertical_word_available_3(){
        WordInfo wordInfo = new WordInfo("HH");
        Integer increment = 5;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode = controller.searchVertical(matrixMap,wordInfo,increment, 25);
        assert (outputFromCode.getStartIndex()==18);
        assert (outputFromCode.getEndIndex()==23);
        assert (outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchVertical_word_available_4(){
        WordInfo wordInfo = new WordInfo("FERLH");
        Integer increment = 5;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode = controller.searchVertical(matrixMap,wordInfo,increment, 25);
        assert (outputFromCode.getStartIndex()==5);
        assert (outputFromCode.getEndIndex()==25);
        assert (outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchVertical_word_available_5(){
        WordInfo wordInfo = new WordInfo("R");
        Integer increment = 5;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode = controller.searchVertical(matrixMap,wordInfo,increment, 25);
        assert (outputFromCode.getStartIndex()==8);
        assert (outputFromCode.getEndIndex()==8);
        assert (outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchVertical_word_not_available_1(){
        WordInfo wordInfo = new WordInfo("FERLHE");
        Integer increment = 5;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode
                = controller.searchVertical(matrixMap,wordInfo,increment, 25);
        assert (!outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchVertical_word_not_available_2(){
        WordInfo wordInfo = new WordInfo("FEEL");
        Integer increment = 5;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode
                = controller.searchVertical(matrixMap,wordInfo,increment, 25);
        assert (!outputFromCode.isWordPresent());
    }
    @Test
    public void test_searchVertical_word_not_available_3(){
        WordInfo wordInfo = new WordInfo("HEAL");
        Integer increment = 5;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode
                = controller.searchVertical(matrixMap,wordInfo,increment, 25);
        assert (!outputFromCode.isWordPresent());
    }
    @Test
    public void test_searchVertical_word_not_available_4(){
        WordInfo wordInfo = new WordInfo("hh");
        Integer increment = 5;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode
                = controller.searchVertical(matrixMap,wordInfo,increment, 25);
        assert (!outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchHorizontal_word_available_1(){
        WordInfo wordInfo = new WordInfo("HAS");
        Integer increment = 1;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode = controller.searchHorizontal(matrixMap,wordInfo,5,25);
        assert (outputFromCode.getStartIndex()==1);
        assert (outputFromCode.getEndIndex()==3);
        assert (outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchHorizontal_word_available_2(){
        WordInfo wordInfo = new WordInfo("RL");
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode = controller.searchHorizontal(matrixMap,wordInfo,5,25);
        assert (outputFromCode.getStartIndex()==19);
        assert (outputFromCode.getEndIndex()==20);
        assert (outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchHorizontal_word_available_3(){
        WordInfo wordInfo = new WordInfo("R");
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode = controller.searchHorizontal(matrixMap,wordInfo,5,25);
        assert (outputFromCode.getStartIndex()==8);
        assert (outputFromCode.getEndIndex()==8);
        assert (outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchHorizontal_word_not_available_1(){
        WordInfo wordInfo = new WordInfo("SDFE");
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode = controller.searchHorizontal(matrixMap,wordInfo, 5,25);
        assert (!outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchDiagonal_word_available_1(){
        WordInfo wordInfo = new WordInfo("HGL");
        Integer increment = 5;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode = controller.searchDiagonal(matrixMap,wordInfo,increment, 25);
        assert (outputFromCode.getStartIndex()==1);
        assert (outputFromCode.getEndIndex()==13);
        assert (outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchDiagonal_word_available_2(){
        WordInfo wordInfo = new WordInfo("MR");
        Integer increment = 5;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode = controller.searchDiagonal(matrixMap,wordInfo,increment, 25);
        assert (outputFromCode.getStartIndex()==9);
        assert (outputFromCode.getEndIndex()==15);
        assert (outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchDiagonal_word_available_3(){
        WordInfo wordInfo = new WordInfo("E H H");
        Integer increment = 5;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode = controller.searchDiagonal(matrixMap,wordInfo,increment, 25);
        assert (outputFromCode.getStartIndex()==11);
        assert (outputFromCode.getEndIndex()==23);
        assert (outputFromCode.isWordPresent());
    }

    @Test
    public void test_searchDiagonal_word_available_4(){
        WordInfo wordInfo = new WordInfo("E");
        Integer increment = 6;
        Map<Integer, String > matrixMap = buildMapFiveByFive();
        WordInfo outputFromCode = controller.searchDiagonal(matrixMap,wordInfo,increment, 25);
        assert (outputFromCode.getStartIndex()==6);
        assert (outputFromCode.getEndIndex()==6);
        assert (outputFromCode.isWordPresent());
    }

    public Map<Integer, String> buildMapFiveByFive(){

        /*
        5x5
        H A S D F
        E G R M E
        E L L P R
        L H H R L
        H U H I H

         */
        Map<Integer, String > matrixMap = new HashMap<>();
        matrixMap.put(1, "H");
        matrixMap.put(2, "A");
        matrixMap.put(3, "S");
        matrixMap.put(4, "D");
        matrixMap.put(5, "F");
        matrixMap.put(6, "E");
        matrixMap.put(7, "G");
        matrixMap.put(8, "R");
        matrixMap.put(9, "M");
        matrixMap.put(10, "E");
        matrixMap.put(11, "E");
        matrixMap.put(12, "L");
        matrixMap.put(13, "L");
        matrixMap.put(14, "P");
        matrixMap.put(15, "R");
        matrixMap.put(16, "L");
        matrixMap.put(17, "H");
        matrixMap.put(18, "H");
        matrixMap.put(19, "R");
        matrixMap.put(20, "L");
        matrixMap.put(21, "H");
        matrixMap.put(22, "U");
        matrixMap.put(23, "H");
        matrixMap.put(24, "I");
        matrixMap.put(25, "H");
        return matrixMap;
    }


    @Test
    public void test_buildMatrixIndex_five_by_five_1(){
        Integer columns = 5;
        Integer index = 3;
        String expectedOutput = "0:2";
        String outputFromCode = controller.buildMatrixIndex(columns, index);
        assert (expectedOutput.equals(outputFromCode));
    }

    @Test
    public void test_buildMatrixIndex_five_by_five_2(){
        Integer columns = 5;
        Integer index = 12;
        String expectedOutput = "2:1";
        String outputFromCode = controller.buildMatrixIndex(columns, index);
        assert (expectedOutput.equals(outputFromCode));
    }

    @Test
    public void test_buildMatrixIndex_five_by_five_3(){
        Integer columns = 5;
        Integer index = 25;
        String expectedOutput = "4:4";
        String outputFromCode = controller.buildMatrixIndex(columns, index);
        assert (expectedOutput.equals(outputFromCode));
    }

    @Test
    public void test_buildMatrixIndex_five_by_five_4(){
        Integer columns = 5;
        Integer index = 1;
        String expectedOutput = "0:0";
        String outputFromCode = controller.buildMatrixIndex(columns, index);
        assert (expectedOutput.equals(outputFromCode));
    }

    @Test
    public void test_buildMatrixIndex_two_by_three_4(){
        Integer columns = 3;
        Integer index = 1;
        String expectedOutput = "0:0";
        String outputFromCode = controller.buildMatrixIndex(columns, index);
        assert (expectedOutput.equals(outputFromCode));
    }

    @Test
    public void test_buildMatrixIndex_two_by_three_5(){
        Integer columns = 3;
        Integer index = 6;
        String expectedOutput = "1:2";
        String outputFromCode = controller.buildMatrixIndex(columns, index);
        assert (expectedOutput.equals(outputFromCode));
    }

    @Test
    public void test_buildMatrixIndex_two_by_three_6(){
        Integer columns = 3;
        Integer index = 4;
        String expectedOutput = "1:0";
        String outputFromCode = controller.buildMatrixIndex(columns, index);
        assert (expectedOutput.equals(outputFromCode));
    }

    @Test
    public void test_buildMatrixIndex_seven_by_five_7(){
        Integer columns = 5;
        Integer index = 4;
        String expectedOutput = "0:3";
        String outputFromCode = controller.buildMatrixIndex(columns, index);
        assert (expectedOutput.equals(outputFromCode));
    }

    @Test
    public void test_buildMatrixIndex_seven_by_five_8(){
        Integer columns = 5;
        Integer index = 34;
        String expectedOutput = "6:3";
        String outputFromCode = controller.buildMatrixIndex(columns, index);
        assert (expectedOutput.equals(outputFromCode));
    }

    @Test
    public void test_buildMatrixIndex_seven_by_five_9(){
        Integer columns = 5;
        Integer index = 22;
        String expectedOutput = "4:1";
        String outputFromCode = controller.buildMatrixIndex(columns, index);
        assert (expectedOutput.equals(outputFromCode));
    }
}
